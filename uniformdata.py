#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 17 14:50:43 2019

@author: nithishreddy
"""

import tensorflow as tf
tf.enable_eager_execution()

raw_dataset = tf.data.TFRecordDataset('training.tfrecords')

raw_dataset

feature_description = {
    'feature0': tf.FixedLenFeature([], tf.int64, default_value=0),
    'feature1': tf.FixedLenFeature([], tf.int64, default_value=0),
    'feature2': tf.FixedLenFeature([], tf.string, default_value=''),
    'feature3': tf.FixedLenFeature([], tf.float32, default_value=0.0),
}

def _parse_function(example_proto):
  # Parse the input tf.Example proto using the dictionary above.
  return tf.parse_single_example(example_proto, feature_description)


parsed_dataset = raw_dataset.map(_parse_function)
parsed_dataset


for parsed_record in parsed_dataset.take(10):
  print(repr(parsed_record))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


import numpy as np

annotations_path = 'training_annotations.txt'

trainL = []
with open(annotations_path,'r') as f:
    for line in f.readlines():
        trainL.append(line.strip())


for i in trainL:

    if len(i.split()[1])>6:
        print(i)


trainlens = [len(i.split()[1]) for i in trainL]

np.sum(np.array(trainlens)==6) , len(trainlens)



for i in trainlens:
    if i>6:
        print(i)


for i in trainL:
    if len(i)>6:
        print(i)




annotations_path = 'testing_annotations.txt'

testL = []
with open(annotations_path,'r') as f:
    for line in f.readlines():
        testL.append(line.strip().split()[1])

testlens = [len(i) for i in testL]

for i in testL:
    if len(i)>6:
        print(i)


import PIL

np.asarray(PIL.Image.open('./datasets/training/image_0.png')).shape



len(trainL) , len(testL)

train_strs = [i.split()[1] for i in trainL]
#test_strs = [i.split()[1] for i in testL]
l = train_strs + testL

from functools import reduce

l = reduce(lambda x,y:x+y , l)

len(set(l))

