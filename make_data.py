# -*- coding: utf-8 -*-

import os,shutil
import xml.etree.ElementTree as ET

path1 = '/Users/nithishreddy/workspace/python/captcha_cracking/vahan_images'
all_files = os.listdir(path1)

all_files = [i for i in all_files if i.endswith('.xml')]
all_files = [i[:-4]+'.png' for i in all_files ]


path = '/Users/nithishreddy/workspace/python/attention-ocr'
train_dataset = 'datasets/training'
test_dataset = 'datasets/testing'

train_dataset_path = os.path.join(path , train_dataset)
test_dataset_path = os.path.join(path , test_dataset)
os.makedirs(train_dataset_path , exist_ok=True)
os.makedirs(test_dataset_path , exist_ok=True)




def collate_text(file1):

    root = ET.parse(file1).getroot()
    l = root.findall('object')
    l = [i.findall('name')[0].text for i in l]
    return ''.join(l)


L_train = [] ; L_test = []
for i_num , file in enumerate(all_files):
    source = os.path.join(path1,file)
    xml_file = os.path.join(path1 , file[:-4]+'.xml')
    str_ = collate_text(xml_file)
    if i_num<=800:
        shutil.copy(source,train_dataset_path)
        L_train.append([file,str_])
    elif i_num>800:
        shutil.copy(source , test_dataset_path)
        L_test.append([file,str_])

len(L_train) ; len(L_test)

# create training file
with open("training_annotations.txt",'w') as f:
    for i in L_train:
        if '%' in i[1]:
            print(i[1])
            i[1] = i[1].replace('%','5')
        f.write('datasets/training/'+i[0]+' '+i[1]+'\n')


with open("testing_annotations.txt",'w') as f:

    for i in L_test:
        if "'" in i[1] or ")" in i[1]:
            print(i[1])
            i[1] = i[1].replace(")","0")
            i[1] = i[1].replace("'" , "")

        f.write('datasets/testing/'+i[0]+' '+i[1]+'\n')


l =[i[1] for i in L_test]

from functools import reduce

l = reduce(lambda x,y:x+y , l)
l = set(l)

for i in list(l):
    print(i)


for i in L_train:
    if not i[1].isalnum():
        print(i[0],i[1])


for i in L_train:
    if len(i[1])>6:
        print(i)

with open("training_annotations.txt",'w') as f:
    for i in L_train:
        f.write('datasets/training/'+i[0]+' '+i[1]+'\n')


with open("testing_annotations.txt",'w') as f:
    for i in L_test:
        f.write('datasets/testing/'+i[0]+' '+i[1]+'\n')







