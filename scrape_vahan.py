import datetime
import os
from selenium.webdriver.chrome.options import Options
import time
from PIL import Image
from selenium import webdriver
import re
from bs4 import BeautifulSoup
import sys
import time

def get_captcha(driver, screen_shot_filename,filename):
    '''
    This functions does:
    1. takes the screenshot of vahan page
    2. crops the captcha text box.
    3. saves the both images as screen_shot_filename and filename
    '''
    driver.set_window_size(1000,1000)
    driver.save_screenshot(screen_shot_filename)

    image = Image.open(screen_shot_filename)

    image = image.crop((310,217,396,247)) # vahan

    image.save(filename ,'png')  # saves new cropped image

def get_captcha_text():
    ''' 
    This functions performs these steps:
    1. reomve log file created by OCR
    2. converts the screenshot saved by get_captcha function into tfrecords format
    3. generates log file (onlinedir.log) from the tfrecords file
    4. scrapes the log file and gets the captcha text to use as input in text box on vahan website.
    '''
    os.system('rm onlinedir.log') # remove the previous log file
    os.system('aocr dataset online_annotations.txt onlinedir.tfrecords --no-force-uppercase')
    os.system('aocr test onlinedir.tfrecords --max-width 86 --max-height 30 --full-ascii --log-path onlinedir.log')
    with open('onlinedir.log','r') as f:
        l = f.readlines()
    s_ = l[-1]
    captcha_text = re.findall('(?<=% )(.*)(?= vs)' , s_)[0][-6:] # use regex to find the output frmo log file
    # last line has captcha text

    print(captcha_text)
    return captcha_text

def main():
    url = 'http://vahan.nic.in/nrservices/faces/user/jsp/SearchStatus.jsp'
    CHROMEDRIVER_PATH = '/home/nithish/chromedriver'
    proj_path = '/home/nithish/workspace/python/attention-ocr'

    options = Options()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--window-size=1200,1100')

    driver = webdriver.Chrome(CHROMEDRIVER_PATH,options = options)
    driver.get(url)

    screen_shot_filename = os.path.join(proj_path , 'screenshot.png')
    filename = os.path.join(proj_path , 'onlinestream.png' ) 
    
    get_captcha(driver , screen_shot_filename , filename)
    
    captcha_text = get_captcha_text()
    
    veh_num = sys.argv[1] #'RJ14CL9099'
    

        
    driver.find_element_by_id('vehiclesearchstatus:regn_no1_exact').send_keys(veh_num)
    driver.find_element_by_id('vehiclesearchstatus:txt_ALPHA_NUMERIC').send_keys(captcha_text)
    driver.find_element_by_name("vehiclesearchstatus:j_id_jsp_664471437_28").click()
    time.sleep(3)
    try:
        e   = driver.find_element_by_xpath("//div[@class='dr-pnl rich-panel ']")
    except:
        raise BaseException('wrong captcha entered')
    e = BeautifulSoup(e.get_attribute('innerHTML'),'html.parser')
    l = e.find_all('td')
    out_l = []
    for i in l:
        try:
            out_l.append(i.text)
        except:
            out_l.append('null')

    print(out_l)
    return out_l


if __name__=='__main__':
    main()

